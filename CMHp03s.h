/*
 * CMHp03s.h
 *
 * Created: 06.04.2015 11:12:57
 *  Author: Christian M�llers
 */ 


#ifndef CMHP03S_H_
#define CMHP03S_H_

float readAirpressureSensor(void);

#endif /* CMHP03S_H_ */