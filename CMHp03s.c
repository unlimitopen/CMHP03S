/*
 * CMHp03s.c
 *
 * Created: 06.04.2015 11:09:20
 *  Author: Christian M�llers
 *
 * This is the first Version for the HP03s, Airpressure Sensor.
 * 
 * Version		Response			Comment
 *	0.01			cm			first release, some sensors have
 *							problem with there values, 
 *
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#include "../CMGlobal.h"

#include <util/delay.h>
#include <string.h>

#include "CMHp03s.h"
#include "../CMTwi.h"

#define F_MCLK      32768
#define OCR1A_VALUE F_CPU / 2 / F_MCLK -1

#define HP03SEEpromWrite	0xA0
#define HP03SEEpromRead		0xA1
#define HP03SADCWrite		0xEE
#define HP03SADCRead		0xEF

#define HP03SNN				60

#define HP03SPIN			PC2
#define HP03SPINOutput		DDRC |= (1<<HP03SPIN)
#define HP03SPINInput		DDRC &=~ (1<<HP03SPIN)
#define HP03SPINHigh		PORTC |= (1<<HP03SPIN)
#define HP03SPINLow			PORTC &=~ (1<<HP03SPIN)
#define HP03SPINToggle		PORTC ^= (1<<HP03SPIN)

#define HP03SMCLKPIN		PB1
#define HP03SMCLKPINOutput	DDRB |= (1<<HP03SMCLKPIN)
#define HP03SMCLKPINInput	DDRB &=~ (1<<HP03SMCLKPIN)
#define HP03SMCLKPINHigh	PORTB |= (1<<HP03SMCLKPIN)
#define HP03SMCLKPINLow		PORTB &=~ (1<<HP03SMCLKPIN)
#define HP03SMCLKPINToggle	PORTB ^= (1<<HP03SMCLKPIN)

#define twWrite 0
#define twRead 1

void masterClockDefine(uint8_t check)
{
	if ( check == TRUE)
	{
		OCR1A   =  0x00;
		TCCR1A  =  0x00;
		TCCR1B  =  0x00;
		
		HP03SMCLKPINOutput;
		OCR1A   =  OCR1A_VALUE;
		TCCR1A  =  (1<<COM1A0);
		TCCR1B  =  (1<<WGM22) |(1<<CS20);
		
	}
	else
	{
		OCR1A   =  0x00;
		TCCR1A  =  0x00;
		TCCR1B  =  0x00;
	}
}

float readAirpressureSensor(void)
{
	HP03SPINOutput;
	HP03SPINLow;
				
	uint16_t MSB;
	uint16_t LSB;
	uint16_t HP03S_C1 = 0;
	uint16_t HP03S_C2 = 0;
	uint16_t HP03S_C3 = 0;
	uint16_t HP03S_C4 = 0;
	uint16_t HP03S_C5 = 0;
	uint16_t HP03S_C6 = 0;
	uint16_t HP03S_C7 = 0;
	uint8_t HP03S_A = 0;
	uint8_t HP03S_B = 0;
	uint8_t HP03S_C = 0;
	uint8_t HP03S_D = 0;
	uint16_t HP03S_D1 = 0;
	uint16_t HP03S_D2 = 0;
	uint8_t i = 0;
	float HP03S_dUT = 0;
	float HP03S_X = 0;
	float HP03S_Sens = 0;
	float HP03S_Off = 0;
	float HP03S_T = 0;
	float HP03S_P = 0;
				
	i2cInit();
	i2cStart(HP03SEEpromWrite);
	i2cSend(0x10);
	i2cStart(HP03SEEpromRead);	
	MSB = i2cReceiveAck();
	LSB = i2cReceiveAck();	
	HP03S_C1 = MSB * 256;
	HP03S_C1 = HP03S_C1 + LSB;

	MSB = i2cReceiveAck();
	LSB = i2cReceiveAck();
	HP03S_C2 = MSB * 256;
	HP03S_C2 = HP03S_C2 + LSB;

	MSB = i2cReceiveAck();
	LSB = i2cReceiveAck();
	HP03S_C3 = MSB * 256;
	HP03S_C3 = HP03S_C3 + LSB;
				
	MSB = i2cReceiveAck();
	LSB = i2cReceiveAck();
	HP03S_C4 = MSB * 256;
	HP03S_C4 = HP03S_C4 + LSB;

	MSB = i2cReceiveAck();
	LSB = i2cReceiveAck();
	HP03S_C5 = MSB * 256;
	HP03S_C5 = HP03S_C5 + LSB;

	MSB = i2cReceiveAck();
	LSB = i2cReceiveAck();
	HP03S_C6 = MSB * 256;
	HP03S_C6 = HP03S_C6 + LSB;

	MSB = i2cReceiveAck();
	LSB = i2cReceiveAck();
	HP03S_C7 = MSB * 256;
	HP03S_C7 = HP03S_C7 + LSB;

	HP03S_A = i2cReceiveAck();
	HP03S_B = i2cReceiveAck();
	HP03S_C = i2cReceiveAck();
	HP03S_D = i2cReceiveNack();
	i2cStop();
			
	masterClockDefine(TRUE);
	HP03SPINOutput;
	HP03SPINHigh;
	
	for (i=0; i<=3; i++)
	{
		i2cInit();
		i2cStart(HP03SADCWrite);
		i2cSend(0xFF);
		i2cSend(0xF0);
		i2cStop();
		_delay_ms(50);
		i2cStart(HP03SADCWrite);
		i2cSend(0xFD);
		i2cStop();
		i2cStart(HP03SADCRead);
		MSB = i2cReceiveAck();
		LSB = i2cReceiveNack();
		i2cStop();				
		HP03S_D1 = MSB * 256;
		HP03S_D1 = HP03S_D1 + LSB;
								
		i2cStart(HP03SADCWrite);
		i2cSend(0xFF);
		i2cSend(0xE8);
		i2cStop();
		_delay_ms(50);
		i2cStart(HP03SADCWrite);
		i2cSend(0xFD);
		i2cStop();
		i2cStart(HP03SADCRead);
		MSB = i2cReceiveAck();
		LSB = i2cReceiveNack();
		i2cStop();
		HP03S_D2 = MSB * 256;
		HP03S_D2 = HP03S_D2 + LSB;	
	}
	masterClockDefine(FALSE);
	HP03SPINLow;
							
	//HP03S_C5 = 32416;
	//HP03S_C5 = 30601;
	
	int x1 = 0;
	int x2 = 0;
				
	x1 = HP03S_D2 - HP03S_C5;
				
	if ( HP03S_D2 >= HP03S_C5 )
	{
		x2 = HP03S_B;
	}
	else
	{
		x2 = HP03S_A;
	}
 
	HP03S_dUT = x1 - (x1 / 128) * (x1 / 128) * x2 / ( 2 << ( HP03S_C ));  // pow(2, HP03S_C)
				
	HP03S_T = 250 + HP03S_dUT * HP03S_C6 / 65536 - HP03S_dUT / ( 2 << ( HP03S_D ));  // pow(2, HP03S_D)
	HP03S_T = HP03S_T / 10;
		
	HP03S_Off = ( HP03S_C2 + (HP03S_C4 - 1024) * HP03S_dUT / 16384 ) * 4;
	HP03S_Sens = HP03S_C1 + HP03S_C3 * HP03S_dUT / 1024;
	HP03S_X = HP03S_Sens * (HP03S_D1 - 7168) / 16384 - HP03S_Off;
	HP03S_P = (HP03S_X * 10 / 32 + HP03S_C7) / 10 ;
	
	return (HP03S_P);	
}